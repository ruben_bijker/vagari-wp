<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<?php
$queried_object = get_queried_object();
$body_classes = 'navbar-is-white ';
if(is_front_page()) {
	$body_classes = 'front-page ';
}
if ($queried_object->taxonomy === 'product_cat') {
	$body_classes .= ' bg-museum is-mobile ';
}
?>
<body <?php body_class($body_classes); ?>>

<?php
/**
 * Logo, search and navbar.
 */
get_template_part( 'views/layout/top-nav' );
