// ScrollMagic setClassToggle multiple FIX
ScrollMagic._util.addClass = function _patchedAddClass(elem, classname) {
  if (classname) {
    if (elem.classList) {
      classname.split(' ').forEach(function _addCls(c) {
        elem.classList.add(c);
      });
    } else elem.className += ' ' + classname;
  }
};
ScrollMagic._util.removeClass = function _patchedRemoveClass(elem, classname) {
  if (classname) {
    if (elem.classList) {
      classname.split(' ').forEach(function _removeCls(c) {
        elem.classList.remove(c);
      });
    } else elem.className = elem.className.replace(new RegExp('(^|\b)' + classname.split(' ').join('|') + '(\b|$)', 'gi'), ' ');
  }
};

jQuery(document).ready(function ($) {

  var browserWidth = $(window).width();
  var footerHeight = $('footer').outerHeight();

  var controller = new ScrollMagic.Controller();

  // BEGIN: THEME TIME OF DAY
  var d = new Date();
  var time = d.getHours();
  const $body = $('body');
  if (time >= 6 && time < 12 ) {
    $body.addClass('theme-grey')
  }
  if (time >= 12 && time < 16) {
    $body.addClass('theme-green')
  }
  if (time >= 16 && time < 21) {
    $body.addClass('theme-red')
  }
  if (time >= 21 && time < 6) {
    $body.addClass('theme-gold')
  }
  // -> check if  theme is in url params
  const urlParams = new URLSearchParams(window.location.search);
  const theme = urlParams.get('theme');
  if (theme !== null) {
    $('body').removeClass().addClass('theme-' + theme + ' front-page');
  }
  // END: THEME TIME OF DAY

  // SECTION_ONE
  var sectionOneBgParallax = $('.section-one__bg-parallax');
  var sectionOneBgParallaxCrown = $('.section-one__crown-top');
  var sectionOneTitle = $('#js_sectionOne__title');
  var sceneSectionOne_parallax = new ScrollMagic.Scene({
    triggerElement: '#js_sectionOne',
    offset: 20,
    triggerHook: 0,
    duration: '100%'
  })
    .setTween(".section-one__bg-parallax", 1, {opacity: 0, ease: Linear.easeNone})
    .addTo(controller);

  var sceneSectionOne_fadeOutTitle = new ScrollMagic.Scene({
    triggerElement: '#js_sectionOne',
    triggerHook: 0,
    offset: 100,
    duration: '50%'
  })
    .setTween("#js_sectionOne__title", 1, {opacity: 0, ease: Linear.easeNone})
    .addTo(controller)
    .on("progress", function (event) {
      var translateY = event.progress * 50;
      sectionOneTitle.css({'transform': 'translateY(' + translateY + '%)'});
    });


  // SECTION_TWO
  var sceneSectionTwo_fadeInCircle = new ScrollMagic.Scene({
    triggerElement: '#js_sectionTwo',
    triggerHook: .4,
    duration: 0,
  })
    .setClassToggle("#js_sectionTwo .animate", "fade-in")
    .addTo(controller);

  var sceneSectionTwo_fadeInElement = new ScrollMagic.Scene({
    triggerElement: '#js_sectionTwo',
    triggerHook: .3,
    duration: 0,
    // reverse: false
  })
    .setClassToggle("#js_sectionTwo .js_animateText", "animate-text--in")
    .addTo(controller);

  // SECTION_THREE -- hand craft
  var sectionThree = $('#js_sectionThree');
  var sceneSectionThree_fadeInElement = new ScrollMagic.Scene({
    triggerElement: '#js_sectionThree',
    triggerHook: .3,
    duration: 0,
  })
    .setClassToggle("#js_sectionThree .js_animateText", "animate-text--in")
    .addTo(controller);


  ////////////////////////
  // SECTION_FOUR //
  var sectionFour = $('#js_sectionFour');

  var sceneSectionFour_elementSlideIn = new ScrollMagic.Scene({
    triggerElement: '#js_sectionFour',
    triggerHook: 1,
    offset: 1,
  })
    .setClassToggle('.element-float-left', 'element-float-in')
    .addTo(controller);

  var sceneSectionFour_fadeInElement = new ScrollMagic.Scene({
    triggerElement: '#js_sectionFour',
    triggerHook: .3,
    duration: 0,
  })
    .setClassToggle("#js_sectionFour .js_animateText", "animate-text--in")
    .addTo(controller);

  // SECTION FIVE
  var sectionFive = $('#js_sectionFive');
  var sectionFiveHeight = sectionFive.outerHeight();
  var sectionFiveTriangleBlur = $('.section-five__triangle__blur');

  var sectionFiveFadeOffset = -5;
  const widthScreenForOffset = 567;
  // if (browserWidth >= widthScreenForOffset) {
  //   sectionFiveFadeOffset = -5;
  // }

  if (browserWidth >= widthScreenForOffset) {
    var tweenSectionFive = TweenMax.from('.section-five__triangle', 1, {bottom: '100vh'});
    var sceneSectionFive_move_triangle = new ScrollMagic.Scene({
      triggerElement: '#js_sectionFive',
      triggerHook: 1,
      offset: footerHeight * 1.5,
    })
      .setTween(tweenSectionFive)
      .addTo(controller);

    var sceneSectionFive_parallax_bg = new ScrollMagic.Scene({
      triggerElement: '#js_sectionFive',
      triggerHook: 1,
      duration: sectionFiveHeight + (footerHeight * 2),
    })
      .addTo(controller)
      .on("progress", function (event) {
        var progress = event.progress;
        var offset = ((1 - progress) * footerHeight * 2);
        var number = progress * footerHeight;
        var positionOffset = number - offset;
        sectionFive.css({'background-position': 'center bottom ' + positionOffset + 'px'});
        sectionFiveTriangleBlur.css({'background-position': 'center bottom ' + positionOffset + 'px'});
      });
  }

  var sceneSectionFive_fadeTriangle = new ScrollMagic.Scene({
    triggerElement: '#js_sectionFive',
    triggerHook: 0,
    offset: sectionFiveFadeOffset
  })
    .setClassToggle("#js_sectionFive .animate--mobile", "fade-in")
    .addTo(controller);

  var sceneSectionFive_contentFadeIn = new ScrollMagic.Scene({
    triggerElement: '#js_sectionFive',
    triggerHook: 0,
    offset: sectionFiveFadeOffset
  })
    .setClassToggle("#js_sectionFive .animate-text--delayed", "animate-text--in")
    .addTo(controller);

  // OTHER JS
  var shapeTriangleProject = $('.shape-triangle-project');
  if (shapeTriangleProject) {
    function setContainerHeight() {
      var width = shapeTriangleProject.innerWidth();
      shapeTriangleProject.closest('.container').height(width * .8);
    }

    setContainerHeight();
    window.addEventListener("resize", setContainerHeight);
  }

  var scrollifyLoadTop = true;
  var scrollifyCurrentIndex = 0;
  $.scrollify({
    section: ".scroll-section",
    scrollSpeed: 1400,
    interstitialSection: ".js_nonFullHeight",
    easing: "easeOutCubic",
  });

  $(".scrollify-next").click(function (e) {
    e.preventDefault();
    $.scrollify.next();
  });

  // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
  let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
  document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
  window.addEventListener('resize', () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  });

  // document.body.style.height = $(window.innerHeight)+'px';

  // END: SCROLL TO NEXT SECTION //////////

});
