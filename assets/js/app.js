// window.$ = window.jquery = jQuery = require('jquery');
var Cookies = require('js-cookie');
const distDir = '/wp-content/themes/v1/dist';
const body = document.body;

if (typeof String.prototype.toCamel !== 'function') {
  String.prototype.toCamel = function(){
    return this.replace(/[-_]([a-z])/g, function (g) { return g[1].toUpperCase(); })
  };
}

jQuery(document).ready(function ($) {

  var isMobile = false; //initiate as false
  if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
    isMobile = true;
  }

  var isMobileElement = $('.is-mobile');

  if (!isMobile) {
    isMobileElement.each(function (e) {
      $(this).removeClass('is-mobile').addClass('is-not-mobile');
    })
  }

  // NAVIGATION NAVBAR
  var menuLink = $('.navbar .nav-link');

  const topNavCollapsible = $('#topNavCollapsible');
  const topNavbar = $('#js_topMenu');
  // };
  topNavCollapsible.on('show.bs.collapse', function () {
    topNavbar.removeClass('navbar-light bg-navbar-dynamic').addClass('navbar-dark bg-primary')
  });
  topNavCollapsible.on('hide.bs.collapse', function () {
    topNavbar.removeClass('navbar-dark bg-primary').addClass('navbar-light bg-navbar-dynamic');
  });

  window.onscroll = function () {
    topNavCollapsible.collapse('hide');
  };

  menuLink.on('click', function () {
    topNavCollapsible.collapse('hide');
  });

  /*
 * Replace all SVG images with inline SVG
 */
  $('img.svg').each(function () {
    var $img = $(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    $.get(imgURL, function (data) {
      // Get the SVG tag, ignore the rest
      var $svg = $(data).find('svg');

      // Add replaced image's ID to the new SVG
      if (typeof imgID !== 'undefined') {
        $svg = $svg.attr('id', imgID);
      }
      // Add replaced image's classes to the new SVG
      if (typeof imgClass !== 'undefined') {
        $svg = $svg.attr('class', imgClass + ' replaced-svg');
      }

      // Remove any invalid XML tags as per http://validator.w3.org
      $svg = $svg.removeAttr('xmlns:a');

      // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
      if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
        $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
      }

      // Replace image with new SVG
      $img.replaceWith($svg);

    }, 'xml');

  });

  function detect() {
    window.setTimeout(function () {
      if (isMobile && window.innerWidth > window.innerHeight && window.innerWidth < 1200) {
        $('#js_rotateScreen').modal('show');
      } else {
        $('#js_rotateScreen').modal('hide');
      }
    }, 200);
  }

  detect();
  window.addEventListener('orientationchange', detect);

  // SPRITE SPIN
  var spritespinElement = $('#js_spritespinWrapper .spritespin');

  if (spritespinElement.length) {
    $.each(spritespinElement, function (key, value) {

      var object = $(this);
      var productId = object.data('product-id');
      object.spritespin({
        source: images360[productId],
        responsive: true,
        sizeMode: 'fit',
        sense: -1,
        animate: false,
        plugins: [
          '360',
          'move',
        ]
      });

    });
  }
  // SPRITE SPIN: END


  // PRODUCT CAROUSEL
  var productOrder = {
    productName: false,
    productDescription: false,
    productImage: false,
    productPrice: false,
    projectName: false,
    artist: false,
    attrColor: false,
    attrColorDescription: false,
    attrSize: false,
    attrExclusion: false,
    attrExclusionDiscount: false,
    attrImg: false,
  };

  var productCarousel = $('#productCarousel')[0];
  var firstProduct = $(productCarousel).find('.carousel-item')[0];
  var initProductOrder = function (productOrder) {
    productOrder.productName = $(productCarousel).data('product-name');
    productOrder.productDescription = $(productCarousel).data('product-description');
    productOrder.productImage = $(productCarousel).data('product-image');
    productOrder.productPrice = $(productCarousel).data('product-price');
    productOrder.projectName = $(productCarousel).data('project-name');
    productOrder.artist = $(productCarousel).data('artist');
    productOrder.attrImg = $(firstProduct).find('img').attr('src');
    productOrder.attrColor = $(firstProduct).data('attr-color');
    productOrder.attrColorDescription = $(firstProduct).data('attr-color-description');
    productOrder.attrExclusion = $(firstProduct).data('attr-exclusion');
    productOrder.attrExclusionDiscount = $(firstProduct).data('attr-exclusion-discount');
  };

  initProductOrder(productOrder);

  $('#js_attrSizes > div').on('click', function (event) {
    var item = $(this);
    console.log('item', item);
    productOrder.attrSize = item.text();
    item.siblings().removeClass("active");
    item.addClass('active');
    updateOrderSummary();
  });

  var reserveButton = $('#js_reserveButton');
  var topNavbarHeight = $(topNavbar).outerHeight();
  var scrollToTopOfForm = function () {
    $([document.documentElement, document.body]).animate({
      scrollTop: $(reserveButton).offset().top - topNavbarHeight - 10
    }, 500);
  };

  $('.js_scrollToTopOfForm').on('click', function () {
    scrollToTopOfForm();
  });

  $(reserveButton).on('click', function (e) {
    if ('1' !== productOrder.attrExclusion && productOrder.attrSize === false) {
      $('#modalSizeInfo').collapse('show');
      e.stopPropagation();
    } else {
      scrollToTopOfForm();
    }
  });

  $(productCarousel).on('slide.bs.carousel', function (e) {
    productOrder.attrColor = e.relatedTarget.dataset.attrColor;
    productOrder.attrColorDescription = e.relatedTarget.dataset.attrColorDescription;
    productOrder.attrExclusion = e.relatedTarget.dataset.attrExclusion;
    productOrder.attrExclusionDiscount = e.relatedTarget.dataset.attrExclusionDiscount;
    productOrder.productPrice = $(productCarousel).data('product-price') - e.relatedTarget.dataset.attrExclusionDiscount;
    productOrder.attrImg = e.relatedTarget.children[0].currentSrc;
    updateOrderSummary();
    if (productOrder.attrExclusion === '1') {
      $('.js_productSizeArea').hide("blind", 300);
      $('.js_productSizeArea2').hide("blind", 300);
      $('.order-form-img__main').addClass('exclusion-attr-dim')
    } else {
      $('.js_productSizeArea').show('fade');
      $('.js_productSizeArea2').show('fade');
      $('.order-form-img__main').removeClass('exclusion-attr-dim');
    }
  });


  var updateOrderSummary = function () {
    var summaryElements = $('.form-order-product-summary [data-order], .js_productDescriptionArea [data-order]');
    $(summaryElements).each(function (i, element) {
      var productKey = $(element).data('order').toCamel();
      if (productKey === 'attrImg') {
        element.src = productOrder[productKey];
      } else {
        element.innerHTML = productOrder[productKey];
      }
    })
  };

  updateOrderSummary();

  var formOrderProductSummary = $('.form-order-product-summary')[0];
  var formOrderFieldsNameEmail = $('.form-order-fields-name-email')[0];
  $(formOrderProductSummary).find('button').on('click', function () {
    $(formOrderProductSummary).hide('fade', function () {
      $(formOrderFieldsNameEmail).show('fade');
    });
  });

  $(formOrderFieldsNameEmail).find('#js_formBack').on('click', function () {
    $(formOrderFieldsNameEmail).hide('fade', function () {
      $(formOrderProductSummary).show('fade');
    });
  });
  // PRODUCT CAROUSEL: END

  // Close modal when clicking outside of it
  $(document).click(function (event) {
    if (!$(event.target).closest(".collapse").length) {
      $(body).find(".collapse").collapse('hide');
    }
  });

  // Add smooth scrolling to mouse icon
  $(".js-smooth-scroll-link").on('click', function (event) {
    event.preventDefault();

    $('html, body').animate({scrollTop: $(this).offset().top}, 800, function () {
    });
  });

  // Submit form with ajax
  $('.wordpress-ajax-form').on('submit', function(e) {
    e.preventDefault();

    var orderData = '';

    var $form = $(this);
    var formData = $form.serialize();

    orderData = formData;

    if(productOrder.productName !== false) {
      var productData = $.param(productOrder);
      orderData += '&' + productData;
    }

    $.ajax({
      type: "post",
      url: $form.attr('action'),
      data: orderData,
      success: function (request) {
        console.log(request);
      }
    });

  });

});