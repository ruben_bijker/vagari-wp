<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php
if ( $posts && count( $posts ) == 1) {
	get_template_part('views/pages/show');
}

if ( $posts && count( $posts ) > 1 ) {
	get_template_part('views/pages/index');
}
?>

<?php endwhile; endif; ?>

<?php get_footer(); ?>
