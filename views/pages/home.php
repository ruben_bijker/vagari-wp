<div class="scroll-section container-wrapper section-one d-flex pb-2" id="js_sectionOne" data-section-name="sectionOne">
    <div class="section-one__bg-parallax">
        <div class="section-one__crown-top"></div>
    </div>
    <div class="container mt-auto">
        <div class="row">
            <div class="col-12 text-center">
                <h1 id="js_sectionOne__title" class="text-center display-1">CREATION</h1>
                <a href="#sectionTwo" class="scrollify-next">
                    <div class="icon-mouse--wrapper text-center">
                        <img class="icon-mouse__mouse" src="<?= get_template_directory_uri() ?>/dist/img/icons/icon-mouse.svg" alt="">
                        <img class="icon-mouse__arrow" src="<?= get_template_directory_uri() ?>/dist/img/icons/icon-arrow-down.svg" alt="">
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="scroll-section container-wrapper section-two" id="js_sectionTwo" data-section-name="sectionTwo">
    <div class="section-two__circle-half animate fade-out"></div>
    <div class="container section-two__content">
        <div class="align-items-center">
            <div class="row justify-content-center">
                <div class="col-8 col-xl-9 text-center">
                    <div class="position-relative mb-0 p-md-0">
                        <p class="display-2 mb-0 animate-text js_animateText">
                            <img src="<?= get_template_directory_uri() ?>/dist/img/icons/qmark-start.svg" alt="" class="qmark-start">The quest to fuse fashion & sculptural arts to forge personalized luxury accessories that carry the imprint of its user.
                            <img src="<?= get_template_directory_uri() ?>/dist/img/icons/qmark-end.svg" alt="" class="qmark-end">
                        </p>
                    </div>
                    <a href="#" class="d-none d-md-inline-block btn btn-primary animate-text js_animateButton">EXPLORE VAGARI
                        <span class="arrow arrow-right"></span></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="scroll-section" data-section-name="sectionThree">
    <div class="container-wrapper section-three d-flex" id="js_sectionThree">
        <div class="section-three__bg-parallax"></div>
        <div class="section-three__content container pt-4 pb-0 py-md-6 my-auto">
            <div class="row align-items-center">
                <div class="col-md-6 pt-3 pb-0 pb-sm-3 text-center text-md-right">
                    <div class="position-relative">
                        <img src="<?= get_template_directory_uri() ?>/dist/img/strap-maker-hands.jpg" alt="" class="img-circle img-fluid section-three__video-circle">
                        <span class="shape-triangle-play"></span>
                    </div>
                </div>
                <div class="col-md-5 py-3 ml-auto animate-text js_animateText section-three__text">
                    <h2 class="mb-2 mb-md-3 text-center text-md-left">
                        <span class="text-white font-smaller">MAKING</span><br/> A VAGARI BELT</h2>
                    <div class="text-center text-md-left">
                        <p class="text-white font-smaller-mobile">Extraordinary pieces bring artist and user together!</p>
                        <p class="text-white font-smaller-mobile">Vagari provides the canvas which artists independently beautify with sculptures that ignite a spirit in every piece.</p>
                        <p class="text-white font-smaller-mobile">The combination of extreme creativity and virtuoso techniques deliver fashion with an unique self-conscious style.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$args  = array(
	'post_type'      => 'project',
	'posts_per_page' => 1,
	'post_status'    => 'publish',
	'meta_query' => array(
		array(
			'key' => 'is_featured',
			'compare' => '=',
			'value' => '1'
		)
	)
);
$project = new WP_Query( $args );
if ( $project->have_posts() ) :
	while ( $project->have_posts() ) : $project->the_post();
		$product_url = '';
		$args_product_from_project  = array(
			'post_type'      => 'product',
			'posts_per_page' => 1,
			'post_status'    => 'publish',
			'meta_query' => array(
				array(
					'key' => 'project',
					'compare' => '=',
					'value' => get_the_ID()
				)
			)
		);
		$product_from_project = new WP_Query( $args_product_from_project );
		if ( $product_from_project->have_posts() ) :
			while ( $product_from_project->have_posts() ) : $product_from_project->the_post();
				$product_url = get_the_permalink();
			endwhile;
		endif;
		$project->reset_postdata();
?>

<div class="scroll-section container-wrapper section-four d-flex pt-i5-4 pt-i6-6 pt-md-6" id="js_sectionFour" data-section-name="sectionFour">
    <div class="container d-flex position-relative mt-md-auto">
        <div class="row element-float-left--wrapper">
            <div class="col-lg-6">
                <img id="js_elementFour" class="element-strap-placement element-float-left" src="<?= get_template_directory_uri() ?>/dist/img/products/strap-zebra.png" alt="">
            </div>
        </div>
        <div class="mt-lg-auto row">
            <div class="col-12 col-md-7 col-xl-6 ml-lg-auto mr-lg-0 mt-lg-auto mx-auto order-2 order-lg-1 p-0 px-md-2 text-center d-flex">
                <div class="w-100 mt-auto d-block d-md-none">
                    <div class="section-four__bg-zebra"></div>
                </div>
                <img src="<?= get_template_directory_uri() ?>/dist/img/zebra-in-shapes.png" alt="" class="d-none d-md-inline mt-auto" style="width: 100%; height: auto; object-fit: contain">
                <a href="<?= $product_url ?>" class="mb-2 mb-i6-25 mb-sm-3 d-lg-none btn btn-primary place-center place-center--bottom">DISCOVER<span class="arrow arrow-right"></span></a>
            </div>
            <div class="col-lg-4 order-1 d-flex order-lg-2 section-four__content" style="z-index: 1">
                <div class="animate-text js_animateText my-auto my-md-0">
                    <h2 class="mb-1 mb-md-2 text-center text-lg-left">
                        <span class="text-white font-smaller">FEATURED PRODUCT</span><br> <?php the_title() ?></h2>
                    <div class="text-hyphens text-center text-sm-left">
                        <p class="text-white  font-smaller-mobile"><?= nl2br( get_the_excerpt() ) ?></p>
                    </div>
                    <a href="<?= $product_url ?>" class="d-none d-lg-inline-flex btn btn-primary mt-2">DISCOVER<span class="arrow arrow-right"></span></a>
                </div>
            </div>
        </div>
    </div>
</div>
	<?php
	endwhile;
endif;
wp_reset_postdata();

$args  = array(
	'post_type'      => 'location',
	'posts_per_page' => 1,
	'post_status'    => 'publish',
	'meta_query' => array(
		array(
			'key' => 'is_featured',
			'compare' => '=',
			'value' => '1'
		)
	)
);
$location = new WP_Query( $args );
if ( $location->have_posts() ) :
	while ( $location->have_posts() ) : $location->the_post();
		$background_image = get_field('background_image');
		$background_image_blur = get_field( 'background_image_blur' );
		?>
        <style>
            .section-five {
                background-image: url(<?= $background_image['url'] ?>);
            }
            .section-five__triangle__blur {
                background-image: url(<?= $background_image_blur['url'] ?>);
            }
        </style>
        <div class="scroll-section bg-black" data-section-name="sectionFive">
            <div class="container-wrapper section-five" id="js_sectionFive">
                <div class="section-five__triangle animate--mobile">
                    <div class="section-five__triangle__blur"></div>
                </div>
                <div class="container w-100-between-md-lg section-five--content-container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-sm-7 col-xl-5 text-center pt-55 pt-sm-6 section-five__text--wrapper" style="z-index: 1">
                            <div class="shape-triangle--wrapper">
                                <div class="animate-text--delayed">
                                    <h2 class="mb-2 mb-md-3 text-dark">
                                        <span class="text-white font-smaller">FEATURED LOCATION</span><br/> <?php the_title() ?>
                                    </h2>
                                    <div class="shape-triangle shape-triangle__left"></div>
                                    <div class="shape-triangle shape-triangle__right"></div>
                                    <p class="section-five__text px-1 mb-3 mb-sm-4 font-smaller-mobile">
										<?= nl2br( get_the_excerpt() ) ?>
                                    </p>
                                    <a href="<?php the_field( 'google_maps_link' ) ?>" class="btn btn-primary" target="_blank">VIEW ON MAP<span class="arrow arrow-right"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php get_template_part( 'views/layout/footer' ) ?>
        </div>
	<?php
	endwhile;
	else: ?>
	<div class="scroll-section bg-black" data-section-name="">
        <?php get_template_part( 'views/layout/footer' ); ?>
    </div>
	<?php
endif;
wp_reset_postdata();
?>

<div class="modal fade" id="js_rotateScreen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <p>For best viewing experience please rotate screen.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>