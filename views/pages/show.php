<article class="container bg-white py-3 py-md-4">
    <div class="row">
        <div class="col-sm-12">
			<?php the_title( '<h1>', '</h1>' ); ?>
			<?php the_content(); ?>
        </div>
    </div>
</article>
