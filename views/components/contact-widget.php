<div class="col-10 col-md-5 text-center">
    <h3 class="text-white">Extreme Personalization?</h3>
    <p class="mt-3">Besides our unique collection, we also run a bespoke service: Vagari Atelier.</p>
    <p>Please contact us to discuss what you are looking for, designer of choice, material and design, its al up to you!</p>
    <p class="mb-3">Delivering artisanal excellence, tailored to your exact requirements.</p>
    <a href="/support/" class="btn btn-primary">Contact Us</a>
</div>