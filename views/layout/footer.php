<footer class="container-fluid bg-black">
	<div class="container">
		<div class="row text-white no-gutters-md-down">
			<div class="col-md-4 text-center text-md-left mb-2 mb-md-0">
				<span style="margin-top: -1px; display: inline-block">&copy;<?= the_date('Y') ?></span>
				<img class="d-inline brand-footer" src="<?= get_template_directory_uri() ?>/dist/img/brand/vagari-logo.svg" alt="">
			</div>
			<div class="col-5 text-right text-md-left col-md-4 col-lg-3 ml-md-auto">
				<?php
				wp_nav_menu( [
					'menu'           => 'footer-menu-1',
					'theme_location' => 'footer',
					'container'      => false,
					'menu_id'        => false,
					'menu_class'     => '',
					'depth'          => 1
				] );
				?>
			</div>
			<div class="col-5 offset-2 offset-md-0 col-md-4 col-lg-3 col-xl-2">
				<?php
				wp_nav_menu( [
					'menu'           => 'footer-menu-2',
					'theme_location' => 'footer',
					'container'      => false,
					'menu_id'        => false,
					'menu_class'     => '',
					'depth'          => 1
				] );
				?>
			</div>
		</div>
	</div>
</footer>