<?php
$queriedObject = get_queried_object();
$classes = is_front_page() || is_tax('product_cat') ? 'navbar-shade' : '' ;
?>
<div class="<?= $classes ?>">
    <nav class="container navbar fixed-top navbar-light bg-navbar-dynamic justify-content-between" id="js_topMenu">
        <button id="js_navbarHamburger" class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#topNavCollapsible" aria-expanded="false" aria-controls="topNavCollapsible" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span> <span class="icon-bar top-bar"></span>
            <span class="icon-bar middle-bar"></span> <span class="icon-bar bottom-bar"></span>
        </button>
        <a href="/" class="navbar-brand"> <img src="<?= get_template_directory_uri() ?>/dist/img/brand/vagari-logo.svg" class="svg" alt=""></a>
        <span style="width: 60px"></span>
        <div class="collapse navbar-collapse" id="topNavCollapsible">
			<?php
			wp_nav_menu( [
				'menu'           => 'top-menu-1',
				'theme_location' => 'top',
				'container'      => false,
				'menu_id'        => false,
				'menu_class'     => 'navbar-nav mr-auto mt-2 mb-3',
				'depth'          => 2,
				'fallback_cb'    => 'bs4navwalker::fallback',
				'walker'         => new WP_Bootstrap_Navwalker()
			] );
			?>
        </div>
    </nav>
</div>