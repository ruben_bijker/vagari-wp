<?php

/**
 * Manage scripts and styles
 */
class enqueue_scripts_styles {
	protected $manifest;

	public function __construct() {
		$this->manifest = $this->get_manifest();

		add_action( 'wp_enqueue_scripts', [ $this, 'styles' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'scripts' ] );
		add_action( 'wp_footer', [ $this, 'dequeue' ] );
	}

	protected function get_manifest() {
		$manifest = file_get_contents( get_template_directory() . '/dist/mix-manifest.json' );

		$manifest = json_decode( $manifest );

		return $manifest;
	}

	protected function mix( $path ) {
		return get_template_directory_uri() . '/dist' . $this->manifest->$path;
	}

	public function styles() {
		wp_enqueue_style( 'app', $this->mix( '/css/app.css' ), [], null );
	}

	public function scripts() {

		wp_enqueue_script(
			'jquery-ui',
			'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js',
			['jquery'],
			null,
			true
		);

		wp_enqueue_script(
			'popper',
			'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js',
			['jquery'],
			null,
			true
		);

		wp_enqueue_script(
			'bootstrap',
			'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',
			['jquery'],
			null,
			true
		);

		wp_enqueue_script( 'app', $this->mix( '/js/app.js' ),
			['jquery', 'bootstrap'],
			true,
		true
			);

		// Only on front page
		if (is_front_page()) {
			wp_enqueue_script(
				'scrollmagic',
				'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js',
				['jquery'],
				null,
				true
			);
			wp_enqueue_script(
				'tweenMax',
				'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.2/TweenMax.min.js',
				['scrollmagic'],
				null,
				true
			);
			wp_enqueue_script(
				'timelineMax',
				'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.0/TimelineMax.min.js',
				['tweenMax'],
				null,
				true
			);
			wp_enqueue_script(
				'scrollmagicAnimations',
				'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/animation.gsap.min.js',
				['timelineMax'],
				null,
				true
			);
			wp_enqueue_script( 'scrollify', get_template_directory_uri() . '/dist/js/scrollify.js',
				['bootstrap'],
				null,
				true
			);
			wp_enqueue_script(
				'scrollmagicIndicators',
				'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/debug.addIndicators.js',
				['timelineMax'],
				null,
				true
			);
			wp_enqueue_script( 'frontpage', $this->mix( '/js/frontpage.js' ),
				['jquery', 'app', 'scrollmagic', 'scrollmagicAnimations', 'tweenMax', 'timelineMax', 'scrollify'],
				true,
				true
			);
		}

		$queried_object = get_queried_object();
		if ($queried_object->taxonomy == 'product_cat') {
			wp_enqueue_script( 'spritespin', get_template_directory_uri() . '/dist/js/spritespin.js',
				['jquery'],
				null,
				false
			);
		}

	}

	public function dequeue() {
		wp_dequeue_script( 'wp-embed' );
	}
}

new enqueue_scripts_styles;