<?php
/**
 * handles form submissions through wp ajax forms.
 */

add_action( 'wp_ajax_custom_action', 'custom_action' );
add_action( 'wp_ajax_nopriv_custom_action', 'custom_action' );
function custom_action() {
	$message = '';

	foreach ( $_POST as $k => $v ) {
		if ( $k == 'action' ) {
			continue;
		}
		$title = ucwords( vagari_from_camel_case( $k ) );
		$message .= $title . ': ' . $v . "\n";
	}

	wp_mail( 'reservation@vagari.art', 'Reservation', $message );

}

function vagari_from_camel_case($camelCaseString) {
	$re = '/(?<=[a-z])(?=[A-Z])/x';
	$a = preg_split($re, $camelCaseString);
	return join($a, " " );
}