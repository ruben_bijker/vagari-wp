<?php

add_action('init', function()
{
    register_post_type('artist', [
        'labels' => [
            'name'          => __('Artists'),
            'singular_name' => __('Artist')
        ],
        'public'      => true,
        'has_archive' => false,
        'menu_position' => 6,
        'rewrite'     => ['slug' => 'artists'],
        'supports'    => ['title', 'editor', 'excerpt', 'thumbnail'],
    ]);

	register_post_type('project', [
		'labels' => [
			'name'          => __('Series'),
			'singular_name' => __('Serie')
		],
		'public'      => true,
		'has_archive' => false,
		'menu_position' => 6,
		'rewrite'     => ['slug' => 'series'],
		'supports'    => ['title', 'editor', 'excerpt', 'thumbnail'],
	]);

	register_post_type('location', [
		'labels' => [
			'name'          => __('Locations'),
			'singular_name' => __('Location')
		],
		'public'      => true,
		'has_archive' => false,
		'menu_position' => 7,
		'rewrite'     => ['slug' => 'locations'],
		'supports'    => ['title', 'excerpt'],
	]);

});