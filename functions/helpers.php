<?php

function get_client_ip() {
	if ( getenv( 'HTTP_CLIENT_IP' ) ) {
		$ipaddress = getenv( 'HTTP_CLIENT_IP' );
	} else if ( getenv( 'HTTP_X_FORWARDED_FOR' ) ) {
		$ipaddress = getenv( 'HTTP_X_FORWARDED_FOR' );
	} else if ( getenv( 'HTTP_X_FORWARDED' ) ) {
		$ipaddress = getenv( 'HTTP_X_FORWARDED' );
	} else if ( getenv( 'HTTP_FORWARDED_FOR' ) ) {
		$ipaddress = getenv( 'HTTP_FORWARDED_FOR' );
	} else if ( getenv( 'HTTP_FORWARDED' ) ) {
		$ipaddress = getenv( 'HTTP_FORWARDED' );
	} else if ( getenv( 'REMOTE_ADDR' ) ) {
		$ipaddress = getenv( 'REMOTE_ADDR' );
	} else {
		$ipaddress = 'UNKNOWN';
	}

	return $ipaddress;
}

function developmentIps() {
	$ips = [
		'127.0.0.1',
		'92.111.148.100',
		'192.168.0.1',
		'85.146.244.194',
		'92.111.69.50'
	];

	return $ips;
}

//add_action( 'init', 'gate_keeper' );
function gate_keeper() {
	$allowedIps      = [[]

	];
	$allowedIps = array_merge($allowedIps, developmentIps()) ;
	$clientsIp       = get_client_ip();
	$allowedEntrance = false;
	foreach ( $allowedIps as $ip ) {
		if ( $ip === $clientsIp ) {
			$allowedEntrance = true;
		}
	}
	if ( ! $allowedEntrance ) {
		die( 'ip not allowed. ask the webmaster to add you ip: ' . $clientsIp );
	}
	return true;
}

add_filter( 'custom_menu_order', 'vagari_custom_menu_order', 10, 1 );
add_filter( 'menu_order', 'vagari_custom_menu_order', 9999999, 1 );
function vagari_custom_menu_order( $menu_order ) {
	if ( ! $menu_order ) {
		return true;
	}

	$menu_item_separator1  = 'separator1';
	$menu_item_product  = 'edit.php?post_type=product';
	$menu_item_page     = 'edit.php?post_type=page';
	$menu_item_post     = 'edit.php';
	$menu_item_comments = 'edit-comments.php';
	$menu_order         = array_diff( $menu_order, [ $menu_item_product, $menu_item_post, $menu_item_comments, $menu_item_page, $menu_item_separator1 ] );

	array_splice( $menu_order, 1, 0, [ $menu_item_page ] );
	array_splice( $menu_order, 2, 0, [ $menu_item_product ] );
	array_splice( $menu_order, 7, 0, [ $menu_item_post ] );

	return $menu_order;
}