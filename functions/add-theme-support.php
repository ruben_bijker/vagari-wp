<?php
/**
 * Add common WordPress theme support tags
 *
 * @link https://developer.wordpress.org/reference/functions/add_theme_support/
 */
class add_theme_support
{
    public function __construct()
    {
        add_action('after_setup_theme', [ $this, 'after_setup_theme']);
        add_action('after_setup_theme', [ $this, 'image_sizes']);
        add_action('after_setup_theme', [ $this, 'nav_menus']);
        add_action('after_setup_theme', [ $this, 'sidebars']);
    }

    public function after_setup_theme()
    {
       add_theme_support( 'title-tag' );
       add_theme_support( 'post-thumbnails' );
    }

    public function image_sizes()
    {
//        add_image_size( 'brand-logo', 113, 85, [ 'center', 'center' ] );
//        add_image_size( 'brand-logo', 200, 200 ); // all brand logo images
//        add_image_size( 'item-medium', 360); // images for overview pages - 3 columns
        add_image_size( '600_600', 600, 600, true ); // item large photo carousel
//        add_image_size( 'item-featured', 720); // uitgelicht news item
//        add_image_size( 'half-container-xl', 750 ); // half page containel xl
//        add_image_size( 'header-large', 1140, 335, true );
//        add_image_size( 'full-width', 1140 );
    }

    public function nav_menus()
    {
        register_nav_menus([
            'top-menu-1'    => 'Top Menu 1',
            'footer-1'      => 'Footer Menu 1',
            'footer-2'      => 'Footer Menu 2',
        ]);
    }

    public function sidebars()
    {
//        register_sidebar( array(
//            'id'          => 'before-footer',
//            'name'        => 'Before Footer Widgets',
//        ) );
    }
}

new add_theme_support;