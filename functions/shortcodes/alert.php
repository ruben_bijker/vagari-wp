<?php
add_shortcode('alert', 'alert_handler');

function alert_handler($atts, $content=null, $code="") {
	extract(shortcode_atts(array('color' => null), $atts) );

	return '<div class="well alert-shortcode" style="background-color:#'.$color.'">
				<p style="color: #ffffff;">'.do_shortcode($content).'</p>
				
			</div>';

}

?>
