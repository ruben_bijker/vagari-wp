<?php
function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

function remove_product_editor() {
	remove_post_type_support( 'product', 'editor' );
}
add_action( 'init', 'remove_product_editor' );