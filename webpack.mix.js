const mix = require('laravel-mix');

mix.setPublicPath('dist')
  .sass('assets/scss/app.scss', 'css')
  .js('assets/js/app.js', 'js')
  .js('assets/js/frontpage.js', 'js')
  .copy(['assets/js/scrollify.js', 'assets/js/spritespin.js'], 'dist/js')
  .copyDirectory('assets/font', 'dist/font')
  .copyDirectory('assets/img', 'dist/img')

  .sourceMaps()
  .version()
  .webpackConfig({
    devtool: 'source-map'
  })
  // .browserSync({
  //   proxy: 'localhost:8027',
  //   files: ['dist/**/*.css', 'dist/**/*.js', '*.php', 'views/**/*.php']
  // })
  .options({
    processCssUrls: false,
  })
;