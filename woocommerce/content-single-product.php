<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;
/** @var $product WC_Product */
global $product;

/** @var WP_Post $project */
$project           = get_field( 'project' );
$extra_image_left  = get_field( 'extra_image_left' );
$extra_image_right = get_field( 'extra_image_right' );

/** @var WP_Post $artist */
$artist        = get_field( 'artist', $project->ID );
$extra_image_1 = get_field( 'extra_image_1', $artist->ID );

$product_attr = $product->get_attributes();

/** @var $strapes WC_Product_Attribute */
$strapes = wc_get_product_terms( $product->get_id(), 'pa_strap', [ 'fields' => 'all' ] );
$sizes   = wc_get_product_terms( $product->get_id(), 'pa_waist-size', [ 'fields' => 'all' ] );

?>

<style>
    <?php
	$row = 0;
	foreach ( $strapes as $strap ) :

    $colors = get_field( 'product_attribute_colors', $strap );

    if(get_field('exclusion_attribute', $strap)) :
    $row ++;
    continue; endif;

    if ( !empty( $colors['inside_color'] ) ) : ?>
        .product-color-circle__item[data-slide-to='<?= $row ?>']:after {
            border-color: <?= $colors['inside_color'] ?>;
            box-shadow: 0 0 0 1px <?= $colors['inside_color'] ?>;
        }

    <?php endif;
	if ( !empty( $colors['outside_color_1'] ) ) : ?>
    .product-color-circle__item[data-slide-to='<?= $row ?>'] {
        box-shadow: 0 0 0 8px <?= $colors['outside_color_1'] ?>;
    }

    <?php endif;

	if ( !empty( $colors['outside_color_2'] ) ) : ?>
    .product-color-circle__item[data-slide-to='<?= $row ?>']:before {
        box-shadow: 0 0 0 8px <?= $colors['outside_color_2'] ?>;
        clip: rect(-50px, 50px, 50px, 17px);
        transform: rotate(45deg);
    }

    <?php
	endif;

	$row ++;
endforeach; ?>
</style>


<div class="bg-primary">
    <div class="container-wrapper container--xl container-wrapper--project-section-one" id="">
        <div class="container-fluid d-flex container-wrapper--project-section-one__content place-center" style="height: inherit; max-height: inherit;">
            <div class="shape-triangle-project shape-triangle-project-beside" style="width: inherit; max-width: inherit"></div>
            <div class="m-auto d-flex flex-column" style="width: fit-content; z-index: 2; position: relative;">
                <h1 class="display-3 mb-0 text-center" style="margin-top: -25%"><?= $project->post_title ?></h1>
                <p class="text-center text-lg-right text-white font-weight-bold">by
                    <span class="text-uppercase"><?= $artist->post_title ?></span></p>
            </div>
        </div>
        <div class="container position-relative h-100">
            <div class="row h-100">
                <div class="col-12 mt-auto pr-md-6">
                    <p class="pb-6 text-primary display-6 font-weight-bold text-center text-md-right">( <?= $product->get_stock_quantity() ?> pieces only )</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg-primary">
    <div class="container-wrapper container--xl container-wrapper--project-section-two" id="project-section-two">
        <div class="container-fluid p-0">
            <div class="row no-gutters justify-content-center">
                <div class="col-md-9 col-xxl-6">
                    <div class="shape-triangle-artist d-none d-md-block"></div>
                    <img src="<?= get_the_post_thumbnail_url( $artist->ID ) ?>" class="shape-circle-img-artist img-circle img-fluid d-none d-md-block" alt="">
                    <div class="shape-drop d-flex">
                        <div class="bg-gradient--2 d-inline d-md-none"></div>
                        <div class="text-center text-md-right px-1 px-md-2 pt-4 pt-md-0 py-xl-1 project-section-two__text">
                            <h3 class="mb-2 mb-md-1 mb-lg-2"><?= $artist->post_title ?></h3>
                            <p><?= wpautop( $artist->post_content ) ?></p>
                        </div>
                        <div class="shape-drop__img d-none d-md-inline">
                            <img src="<?= $extra_image_1['sizes']['600_600'] ?>" alt="" class="img-circle img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row no-gutters d-flex d-md-none" style="overflow: hidden">
                <div class="col-6">
                    <div class="shape-triangle-artist shape-triangle-artist--half-screen"></div>
                </div>
                <div class="col-6">
                    <img src="<?= get_the_post_thumbnail_url( $artist->ID ) ?>" class="img-circle" alt="" style="width: 200%; max-width: 200%">
                </div>
            </div>
        </div>
    </div>

    <div class="container-wrapper container--xl container-wrapper--project-section-three--mobile d-block d-md-none">
        <div class="container-fluid px-0">
            <div class="row" style="background-color: #d9bc93">
                <div class="col-12 py-2">
                    <div class="bg-gradient--1"></div>
                    <img src="<?= $extra_image_left['url'] ?>" alt="" class="img-fluid position-relative">
                    <h4 class="text-center my-0 position-relative">The Panku Project</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 pt-2" style="background-color: #7a716c">
                    <div class="mt-1 px-1 text-center text-md-right">
                        <div class="text-white font-smaller-mobile">
							<?php
							$content = apply_filters( 'the_content', $project->post_content );
							echo $content; ?>
                        </div>
                    </div>
                    <img src="<?= $extra_image_right['url'] ?>" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>

    <div class="container-wrapper container--xl container-wrapper--project-section-three d-none d-md-block">
        <div class="container" style="z-index: 1">
            <div class="row justify-content-end">
                <div class="col-md-6">
                    <div class="pl-2 pl-lg-4 py-4">
                        <h4><?= $project->post_title ?></h4>
                        <div class="text-white font-smaller-mobile">
							<?php
							$content = apply_filters( 'the_content', $project->post_content );
							echo $content; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid position-absolute" style="top: 0; z-index: -1">
            <div class="row">
                <div class="col-md-6" style="background-color: #d9bc93;">
                    <div class="bg-gradient--1"></div>
                    <div class="bg-strap-face bg-strap-face--outer" style="background-image: url(<?= $extra_image_left['url'] ?>"></div>
                    <div class="shape-triangle-strap-face" style="background-color: #897d73"></div>
                </div>
                <div class="col-md-6 bg-strap-face pb-7" style="background-color: #897d73; background-image: url(<?= $extra_image_right['url'] ?>">
                </div>
            </div>
        </div>
    </div>

    <div class="bg-primary container-wrapper container--xl container-wrapper--project-section-four pt-4 pt-md-5">
        <span class="bg-gradient--2"></span>
        <div class="container mb-3 mb-md-5">
            <div class="row">
                <div class="col-12">
                    <h3 class="text-white text-center my-0">Select Your Strap</h3>
                </div>
            </div>
        </div>
        <div id="productCarousel" class="carousel slide carousel-fade"
                data-product-name="<?= $product->get_name() ?>"
                data-product-description="<?= get_field('acf_short_description', $product->get_id()) ?>"
                data-project-name="<?= $project->post_title ?>"
                data-artist="<?= $artist->post_title ?>"
                data-product-price="<?= $product->get_price() ?>"
                data-product-image="<?php the_post_thumbnail_url() ?>">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <img src="<?= get_field( 'carousel_placeholder' )['url'] ?>" alt="" class="item-over-carousel">
                        <div class="carousel-inner">

							<?php
							$active = 'active';
							$row    = 0;
							foreach ( $strapes as $strap ) :
							$strap_img_url        = get_field( 'product_attr_image', $strap )['url'];
							$exclusion_attr_class = get_field( 'exclusion_attribute', $strap ) ? 'exclusion-attr-dim' : '';
							?>

                                <div class="carousel-item <?= $active ?>" data-attr-price=""
                                        data-attr-color="<?= $strap->name ?>"
                                        data-attr-color-description="<?= $strap->description ?>"
                                        data-attr-exclusion="<?= get_field( 'exclusion_attribute', $strap ) ?>"
                                        data-attr-exclusion-discount="<?= get_field( 'exclusion_attribute_discount', $strap ) ?>">
                                    <img src="<?= $strap_img_url ?>" alt="" class="img-fluid <?= $exclusion_attr_class ?>">
                                </div>

								<?php
								$active = '';
								$row ++;
							endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="row mt-2 mt-md-4 justify-content-center">
                    <div class="col-md-10 px-0 px-lg-5 d-flex justify-content-center" style="overflow: hidden">
                        <div class="product-color-circle__outer-wrapper">
                            <div class="d-flex flex-wrap justify-content-center product-color-circle mt-3 mx-1 carousel-indicators">
								<?php
								$active = 'active';
								$row    = 0;
								$no_attr_class = '';
								foreach ( $strapes as $strap ) :
									$no_attr_class = get_field('exclusion_attribute', $strap) ? 'product-color-circle__no-attr' : '' ?>
                                    <span data-target="#productCarousel" data-slide-to="<?= $row ?>" class="<?= $active ?> product-color-circle__item <?= $no_attr_class ?>">
                                        <span class="active-underline"></span>
                                    </span>
									<?php
									$row ++;
									$active = '';
								endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container mt-2 mb-5 my-md-3">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8 text-center hr-bottom pb-5">
                        <div class="js_productDescriptionArea mb-3">
                            <span class="product-variation-name" data-order="attr-color"></span>
                            <p data-order="attr-color-description"></p>
                        </div>
                        <div class="js_productSizeArea">
                            <a data-toggle="collapse" data-target="#modalSizeInfo" style="cursor: pointer"><span class="text-white">Size</span><span class="bg-circle ml-2"><span style="z-index: 1; top: 2px; position: relative;">?</span></span></a>

                            <div class="collapse bg-white" id="modalSizeInfo">
                                <button type="button" class="close mr-1 " data-toggle="collapse" data-target="#modalSizeInfo" aria-label="Close" style="font-size: 40px; margin-top: 5px">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="p-2">
                                    <span data-toggle="collapse" data-target="#collapseReserveForm"></span>
                                    <p><b>YOUR SIZE</b></p>
                                    <p>You have to choose an original out of the box "snug tight close-fit"</p>
                                    <p>To determine your size, measure your favourite, well-fitting belt from the buckle to the hole you use most.</p>
                                    <p>Lay the belt on a flat surface. Measure from the hole you use most to the end of the belt, where it meets the buckle.</p>
                                    <img src="<?= get_template_directory_uri() ?>/dist/img/belt-size-measure.jpg" alt="">
                                    <p class="mt-2"><strong>ADVISE!</strong><br/>
                                        Over time, leather belts tend to stretch. It’s good to keep in mind that a belt that fits snug initially will likely wear more comfortably down the road.</p>
                                    <p>When measuring your belt keep in mind that you have to choose an original out of the box “snug tight close-fitting’ hole.</p>
                                </div>
                            </div>

                            <div class="size-list__wrapper">
                                <div class="size-list mt-2" id="js_attrSizes">
			                        <?php foreach ( $sizes as $size ) : ?>
                                        <div class="size-list__item"><?= $size->name ?></div>
			                        <?php endforeach; ?>
                                </div>
                            </div>

                        </div>
                        <div class="mb-4"></div>
                        <button type="button" id="js_reserveButton" class="btn btn-primary position-relative" data-toggle="collapse" data-target="#collapseReserveForm" style="z-index: 1">Reserve</button>
                        <div class="collapse bg-light position-relative" id="collapseReserveForm" style="z-index: 10; top: -39px;">
                            <button type="button" class="close mr-1" data-toggle="collapse" data-target="#collapseReserveForm" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="py-4 px-2 px-md-5">
                                <span data-toggle="collapse" data-target="#collapseReserveForm"></span>
                                <h1 class="text-dark mb-3">Reservation</h1>

                                <!--form-order-product-summary-->
                                <div class="form-order-product-summary">
                                    <div class="position-relative mt-2 mb-4 px-1 px-md-5">
                                        <div class="order-form-img">
                                            <img src="" alt="" class="order-form-img__main" data-order="attr-img">
                                            <img src="<?= get_field( 'carousel_placeholder' )['url'] ?>" alt="" class="order-form-img__placeholder" id="js_orderFormImgPlaceholder">
                                        </div>
                                    </div>
                                    <p class="display-5 mb-0" data-order="project-name"></p>
                                    <p class="mb-0" data-order="artist"></p>
                                    <p class="" data-order="product-description"></p>
                                    <p class="display-5 my-2">+</p>
                                    <p class="display-5 mb-0" data-order="attr-color"></p>
                                    <p class="mb-0" data-order="attr-color-description"></p>
                                    <p class="js_productSizeArea2">Size: <span data-order="attr-size"></span></p>
                                    <p class="display-5 mt-3 mb-0"><b>US$ <span data-order="product-price"></span></b></p>
                                    <p>(<?= $product->get_stock_quantity() ?> pieces only)</p>

                                    <div class="form-group row justify-content-center mt-3">
                                        <button type="button" class="btn btn-primary js_scrollToTopOfForm">Next<span class="arrow arrow-right"></span>
                                        </button>
                                    </div>
                                </div>

                                <!--form-order-fields-name-email-->
                                <div class="form-order-fields-name-email" style="display: none">
                                    <p class="">We need a bit of information from you so we can contact you and complete the order.</p>
                                    <form class="wordpress-ajax-form" method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
                                    <div class="form-group row">
                                            <label for="fullName" class="col-sm-5 text-md-right col-form-label my-auto">FULL NAME</label>
                                            <div class="col-sm-7 pl-md-0">
                                                <input type="text" name="fullName" class="form-control" id="fullName" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="emailAddress" class="col-sm-5 text-md-right col-form-label my-auto">EMAIL ADDRESS</label>
                                            <div class="col-sm-7 pl-md-0">
                                                <input type="email" name="email" class="form-control" id="emailAddress" required>
                                            </div>
                                        </div>
                                        <div class=" mt-3">
                                            <p>You will receive a copy of your reservation in your mailbox, and we will contact you as soon as possible.</p>
                                            <p>Thanks,</p>
                                            <p>the VAGARI team</p>
                                        </div>
                                        <input type="hidden" name="action" value="custom_action">
                                        <div class="form-group row justify-content-center mt-3">
                                            <button type="button" class="btn btn-primary mr-2 js_scrollToTopOfForm" id="js_formBack">
                                                <span class="arrow arrow-left"></span>Back
                                            </button>
                                            <button type="submit" class="btn btn-primary">Send<span class="arrow arrow-right"></span>
                                            </button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center mt-5">
                    <?php get_template_part( 'views/components/contact-widget' ) ?>
                </div>
            </div>
        </div>
    </div>
</div>