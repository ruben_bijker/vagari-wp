<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

get_header();

?>
    <div class="container my-3 my-md-5">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <h1 class="mb-1 mb-md-3">THE TREASURY</h1>
                <!--                <h1 class="">--><?php //woocommerce_page_title(); ?><!--</h1>-->
	            <?php
	            do_action( 'woocommerce_archive_description' );
	            ?>
                <p class="mb-0">Two choices that define your style. <br/><br/>Choose Your Face <br/>Select le Strap
                </p>
            </div>
        </div>
    </div>
    <div class="w-100 bg-white-semi-transparent my-5 d-none d-md-block">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex flex-nowrap material-sample--wrapper">
                    <span class="material-sample" style="border-bottom-color: #916427"><img class="img-fluid" src="<?= get_template_directory_uri() ?>/dist/img/products/strap-samples/1.jpg"></span>
                    <span class="material-sample" style="border-bottom-color: #903c1c"><img class="img-fluid" src="<?= get_template_directory_uri() ?>/dist/img/products/strap-samples/2.jpg"></span>
                    <span class="material-sample" style="border-bottom-color: #97429d"><img class="img-fluid" src="<?= get_template_directory_uri() ?>/dist/img/products/strap-samples/3.jpg"></span>
                    <span class="material-sample" style="border-bottom-color: #0b2e13"><img class="img-fluid" src="<?= get_template_directory_uri() ?>/dist/img/products/strap-samples/4.jpg"></span>
                    <span class="material-sample" style="border-bottom-color: #539188"><img class="img-fluid" src="<?= get_template_directory_uri() ?>/dist/img/products/strap-samples/5.jpg"></span>
                    <span class="material-sample" style="border-bottom-color: #92822c"><img class="img-fluid" src="<?= get_template_directory_uri() ?>/dist/img/products/strap-samples/6.jpg"></span>
                    <span class="material-sample" style="border-bottom-color: #448c87"><img class="img-fluid" src="<?= get_template_directory_uri() ?>/dist/img/products/strap-samples/7.jpg"></span>
                    <span class="material-sample" style="border-bottom-color: #b22d34"><img class="img-fluid" src="<?= get_template_directory_uri() ?>/dist/img/products/strap-samples/8.jpg"></span>
                    <span class="material-sample" style="border-bottom-color: #425ba6"><img class="img-fluid" src="<?= get_template_directory_uri() ?>/dist/img/products/strap-samples/9.jpg"></span>
                </div>
            </div>
        </div>
    </div>

<?php
global $images_360_urls;
$images_360_urls = [];
if ( woocommerce_product_loop() ) { ?>
    <div class="container" id="js_spritespinWrapper">
        <div class="row">
			<?php
			if ( wc_get_loop_prop( 'total' ) ) {
				while ( have_posts() ) {
					the_post();
					wc_get_template_part( 'content', 'product' );
				}
			}
			?>
        </div>
    </div>
    <script>
      var images360 = <?= json_encode($images_360_urls) ?>;
    </script>
	<?php
} else {
	do_action( 'woocommerce_no_products_found' );
} ?>

    <div class="container my-6">
        <div class="row justify-content-center">
			<?php get_template_part( 'views/components/contact-widget' ) ?>
        </div>
    </div>
</div>

<?php
get_footer();