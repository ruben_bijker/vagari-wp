<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

/** @var $product WC_Product */
global $product;
global $images_360_urls;

if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

$project    = get_field( 'project' );
$artist     = get_field( 'artist', $project->ID );
$images_360 = get_field( '360_images' );

foreach ( $images_360 as $img ) {
	$images_360_urls[$product->get_id()][] = $img['url'];
}
?>

<script>
  //images360 = <?//= json_decode($images_360_urls) ?>//;
</script>
<div <?php wc_product_class('col-md-6 col-xl-4 text-center mb-2'); ?>>
    <div class="bg-white-semi-transparent p-2 product-block--wrapper">
        <div class="px-2">
            <div class="spritespin product-block__img" data-product-id="<?= $product->get_id() ?>"></div>
        </div>
        <h4 class="product-block__title"><?= $product->get_name() ?></h4>
        <div class="product-block__description">
            <p style="margin-bottom: 5px"><?= $artist->post_title ?></p>
            <p><?= get_field('acf_short_description') ?></p>
        </div>
        <p class="product-block__price"><b>US&#36; <?= $product->get_price() ?></b>
            <span class="product-block__description">(<?= $product->get_stock_quantity() ?> pieces only)</span></p>
        <a href="<?= $product->get_permalink() ?>" class="btn btn-primary my-1">DISCOVER<span class="arrow arrow-right"></span></a>
    </div>
</div>
