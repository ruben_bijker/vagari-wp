<?php
/**
 * Nothing more then a require_once document for all functions.
 */
require_once('functions/add-theme-support.php');
require_once('functions/bs4-nav-walker.php');
require_once('functions/enqueue-scripts-styles.php');
require_once('functions/form-submit-ajax.php');
//require_once('functions/nav-top-walker.php');
//require_once('functions/remove_emoji.php');
require_once('functions/register-post-types.php');
require_once('functions/wp-head-favicons.php');
//require_once('functions/shortcodes.php');
require_once('functions/helpers.php');
require_once('functions/woocommerce.php');